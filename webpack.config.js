var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */

    // Add JS Files Import
    .addEntry('js/chosen/chosen', './assets/js/chosen/chosen.js')
    .addEntry('js/chosen/chosen.jquery', './assets/js/chosen/chosen.jquery.js')
    .addEntry('js/chosen/chosen.jquery.min', './assets/js/chosen/chosen.jquery.min.js')
    .addEntry('js/chosen/chosen.proto', './assets/js/chosen/chosen.proto.js')
    .addEntry('js/chosen/chosen.proto.min', './assets/js/chosen/chosen.proto.min.js')

    .addEntry('js/prism', './assets/js/prism.js')
    .addEntry('js/init', './assets/js/init.js')
    .addEntry('js/init.proto', './assets/js/init.proto.js')
    .addEntry('js/TimeCircles', './assets/js/TimeCircles.js')
    .addEntry('js/bootstrap-notify', './assets/js/bootstrap-notify.min.js')


    .addEntry('js/jquery-3.2.1.min', './assets/js/jquery-3.2.1.min.js')
    .addEntry('js/datatable', './assets/js/datatable.js')
    .addEntry('js/prototype-1.7.0.0', './assets/js/prototype-1.7.0.0.js')


    // Permet de recuperer les images du dossier image dans ./assets/image/
    .addEntry('js/script/image', './assets/js/recursifEncore.js')
    // Add JS Files Perso
    .addEntry('js/app', './assets/js/app.js')
    .addEntry('js/script/multiForm', './assets/js/script/multiForm.js')
    .addEntry('js/script/saveIdea', './assets/js/script/saveIdea.js')
    .addEntry('js/script/resetpassword', './assets/js/script/resetpassword.js')
    .addEntry('js/script/addParticipants', './assets/js/script/addParticipants.js')
    .addEntry('js/script/profil', './assets/js/script/profil.js')
    .addEntry('js/script/session', './assets/js/script/session.js')
    .addEntry('js/script/getCalendar', './assets/js/script/getCalendar.js')
    .addEntry('js/script/modal', './assets/js/script/modal.js')
    .addEntry('js/script/chat', './assets/js/script/chat.js')
    .addEntry('js/script/timer', './assets/js/script/timer.js')
    .addEntry('js/script/atelier', './assets/js/script/atelier.js')
    .addEntry('js/script/responsive', './assets/js/script/responsive.js')

    // Add CSS Files Import
    .addStyleEntry('css/import/chosen', './assets/css/import/chosen.css')
    .addStyleEntry('css/import/chosen.min', './assets/css/import/chosen.min.css')
    .addStyleEntry('css/import/prism', './assets/css/import/prism.css')
    .addStyleEntry('css/import/bootswatch.min', './assets/css/import/bootswatch.min.css')
    .addStyleEntry('css/import/hover.min', './assets/css/import/hover-min.css')
    .addStyleEntry('css/import/TimeCircles', './assets/css/import/TimeCircles.css')
    .addStyleEntry('css/import/animate.min', './assets/css/import/animate.min.css')

    // Add CSS Files Perso
    .addStyleEntry('css/stylesheet', './assets/css/stylesheet.css')
    .addStyleEntry('css/fullcalendar', './assets/css/fullcalendar.css')
    .addStyleEntry('css/profil', './assets/css/profil.css')
    .addStyleEntry('css/atelier', './assets/css/atelier.css')
    .addStyleEntry('css/modal', './assets/css/modal.css')
    .addStyleEntry('css/chat', './assets/css/chat.css')
    .addStyleEntry('css/errors', './assets/css/errors.css')
    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables Sass/SCSS support
    //.enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()
    .configureFilenames({
        images: 'image/[name].[ext]'
});

var config = Encore.getWebpackConfig();

config.externals.jquery = 'jQuery';

module.exports = config;
