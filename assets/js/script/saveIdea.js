//permet de recupérer et de d'afficher la liste des idées d'un atelier
window.getIdeaListe = function(){
    var id = $(".atelier").attr('id');
    $.ajax({
        url: window.location.origin + "/get/idea/liste/atelier/" + id,
        type: "GET",

        success: function (data) {
            //reconstruction de la liste
            var idees = data;
            $("#ideaListe").empty();
            $("#ideaListe").append($("<ol>").attr("id", "liste"));
            for (var i in idees){
                var idee = idees[i];
                $("#liste").append($("<li>").addClass("idee").text(idee.intitule));
            }
        },
    });
};

//permet d'ajouter une idée en base de donnée
window.addIdea = function(id){
    if ($("#idee").val() !== "") {
        $.ajax({
            url: window.location.origin + "/add/idea/atelier/" + id,
            type: "POST",
            dataType: "json",

            data: {
                nom: $("#idee").val()
            },

            success: function () {
                //vide le textarea
                $("#idee").val("");
                //refresh de la liste des idées
                getIdeaListe();
            }
        });
    }
};