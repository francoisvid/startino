//permet de mettre à jour les informations d'un admin
window.updateProfil = function(e) {
    e.preventDefault();
    if ($('#password').val() !== "") {
        data = {
            nom: $('#nom').val(),
            prenom: $('#prenom').val(),
            email: $('#email').val(),
            entreprise: $('#entreprise').val(),
            password: $('#password').val(),
        };
    } else {
        data = {
            nom: $('#nom').val(),
            prenom: $('#prenom').val(),
            email: $('#email').val(),
            entreprise: $('#entreprise').val(),
        };
    }

    $.ajax({
        type: 'POST',
        url: window.location.origin + "/update/profil",
        data: data,
        success: function() {
            //affichage d'une notification qui confirme la ou les modifications
            $.notifyDefaults({
                placement: {
                    from: "top",
                    align: "center"
                },
                offset: {
                    y: 55
                },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                type: 'success',
                allow_dismiss: false
            });
            $.notify('Votre profil à bien été mis à jour !');
        },
        error: function() {
            //affichage d'une notification signalant une erreur
            $.notifyDefaults({
                placement: {
                    from: "top",
                    align: "center"
                },
                offset: {
                    y: 55
                },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                type: 'danger',
                allow_dismiss: false
            });
            $.notify('Une erreur est survenue, merci de réesayer !');
        }
    });
};

$(document).ready(function() {
    //cette algo permet d'afficher le mot de passe en cliquant sur l'oeil et de changer l'état de l'oeil
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if ($('#show_hide_password input').attr("type") == "text") {
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass("fa-eye-slash");
            $('#show_hide_password i').removeClass("fa-eye");
        } else if ($('#show_hide_password input').attr("type") == "password") {
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass("fa-eye-slash");
            $('#show_hide_password i').addClass("fa-eye");
        }
    });
});