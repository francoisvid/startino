//initialisation du booleen qui permettra de lancer le timer
var start = false;

//permet d'afficher le timer
window.timerCircle = function() {
    if(start) {
        $("#DateCountdown").TimeCircles({
            "animation": "smooth",
            "bg_width": 0.4,
            "fg_width": 0.05333333333333334,
            "circle_bg_color": "#DA6552",
            "time": {
                "Days": {
                    "text": "Days",
                    "color": "#FFCC66",
                    "show": false
                },
                "Hours": {
                    "text": "Hours",
                    "color": "#99CCFF",
                    "show": true
                },
                "Minutes": {
                    "text": "Minutes",
                    "color": "#BBFFBB",
                    "show": true
                },
                "Seconds": {
                    "text": "Seconds",
                    "color": "#FF9999",
                    "show": true
                }
            }
        });
        $("#btn-go").hide();
    }
};

//permet de lancer le timer
window.lunch = function() {
    start = true;
    timerCircle();
};

