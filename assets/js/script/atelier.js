var $ = require('jquery');

//supprimer un facilitateur d'un atelier
window.removeFacilitateur = function (idA, idF) {
    $.ajax({
        url:  window.location.origin + "/delete/atelier/" + idA +"/facilitateur/" + idF,
        type: "POST",
        success: function () {
            document.location.reload();
        },
        error: function () {
            console.log("erreur delete facilitateur carousel")
        }
    });
}