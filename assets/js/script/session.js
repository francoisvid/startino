var $ = require('jquery');
//permet d'envoyer une session en base de données
window.createSession = function(){
    //affichage d'une notification qui confirme la création de la session
    $.notifyDefaults({
        placement: {
            from: "top",
            align: "center"
        },
        // offset: {
        //     y : 55
        // },
        spacing: 10,
        z_index: 3000,
        delay: 2000,
        timer: 500,
        type: 'danger',
        allow_dismiss: false
    });
    //affichage d'une notification spécifique si ces 3 conditions ne sont pas remplies
    if($("#nomSession").val() !== "") {
        if($("#dateDSession").val() !== "" && $("#dateFSession").val() !== "") {
            if($("#dateDSession").val() <= $("#dateFSession").val()) {
                $.ajax({
                    url: window.location.origin + "/session/create",
                    type: "POST",
                    data: {
                        nom: $("#nomSession").val(),
                        debut: $("#dateDSession").val(),
                        fin: $("#dateFSession").val()
                    },
                    success: function (data) {
                        document.location.reload();
                    },
                });
            }else{
                $.notify("La date de fin ne doit pas être inférieur à la date de début");
            }
        }else{
            $.notify("Veuillez séléctionner une date de début et une date de fin");
        }
    }else{
        $.notify("Veuillez nommer votre session");
    }
};

window.validateSession = function (id, e) {
    e.preventDefault()
    $.ajax({
        url:  window.location.origin + "/validate/session/" + id,
        type: "POST",
        success: function (data) {
            $.notifyDefaults({
                placement: {
                    from: "top",
                    align: "right"
                },
                // offset: {
                //     y : 55
                // },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                type: 'success',
                allow_dismiss: false
            });
            $('.hideModal').modal('hide');
            $.notify("La session a été validée avec succès !");
            setTimeout(function(){
                window.location.reload(1);
            }, 1000);
            // document.location.reload();
        },
        error: function () {
            $.notifyDefaults({
                placement: {
                    from: "top",
                    align: "right"
                },
                // offset: {
                //     y : 55
                // },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                type: 'danger',
                allow_dismiss: false
            });
            $('.hideModal').modal('hide');
            $.notify("Une erreur est survenue, merci de réessayer !");
            setTimeout(function(){
                window.location.reload(1);
            }, 1000);
            console.log("erreur validation session")
        }
    });
};

window.deleteSeance = function (id) {
    $.ajax({
        url:  window.location.origin + "/delete/seance/" + id,
        type: "POST",

        success: function (data) {
            document.location.reload();
        },
        error: function () {
        }
    });
};

window.deleteSession = function (id, e) {
    e.preventDefault();
    $.ajax({
        url:  window.location.origin + "/delete/session/" + id,
        type: "POST",

        success: function (data) {
            // document.location.reload();
            $.notifyDefaults({
                placement: {
                    from: "top",
                    align: "right"
                },
                // offset: {
                //     y : 55
                // },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                type: 'warning',
                allow_dismiss: false
            });
            $('.hideModal').modal('hide');
            $.notify("La session a été supprimer avec succès !");
            setTimeout(function(){
                window.location.reload(1);
            }, 1000);
        },
        error: function () {
            $.notifyDefaults({
                placement: {
                    from: "top",
                    align: "right"
                },
                // offset: {
                //     y : 55
                // },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                type: 'danger',
                allow_dismiss: false
            });
            $('.hideModal').modal('hide');
            $.notify("Une erreur est survenue, merci de réessayer !");
            setTimeout(function(){
                window.location.reload(1);
            }, 1000);
        }
    });
};

window.addFtA = function (id) {
    console.log(id);
    console.log($("#ateliers"+id).val());
    $.ajax({
        url: window.location.origin + "/add/atelier/facilitateur/" + id,
        type: "POST",
        data:{
            ateliers : $("#ateliers"+id).val()
        },
        success: function (data) {
            document.location.reload();
        },
        error: function () {
        }
    });
};

window.updateSeance = function (id) {
    $.ajax({
        url:  window.location.origin + "/update/seance/"+id,
        type: "POST",

        data: {
            nom: $("#nom"+id).val(),
            description: $("#description"+id).val(),
            adresse: $("#adresse"+id).val(),
            heure_debut: $("#heure_debut"+id).val(),
            heure_fin: $("#heure_fin"+id).val()
        },

        success: function (data) {
            document.location.reload();
        },
        error: function () {
        }
    });
};

window.annulerSession = function (id, e) {
    e.preventDefault();
    $.ajax({
        url:  window.location.origin + "/remove/session/" + id,
        type: "POST",

        success: function (data) {
            // document.location.reload();
            $.notifyDefaults({
                placement: {
                    from: "top",
                    align: "right"
                },
                // offset: {
                //     y : 55
                // },
                spacing: 10,
                z_index: 1031,
                delay: 2000,
                timer: 500,
                type: 'warning',
                allow_dismiss: false
            });
            $('.hideModal').modal('hide');
            $.notify("La session a été annuler avec succès !");
            setTimeout(function(){
                window.location.reload(1);
            }, 1000);
        },
        error: function () {
        }
    });
};

window.updateSession = function (id) {
    $.ajax({
        url:  window.location.origin + "/update/session/" + id,
        type: "POST",
        data: {
            nomm: $('#nomm').val(),
            date_debut: $('#date_debut').val(),
            date_fin: $('#date_fin').val()
        },
        success: function (data) {
            document.location.reload();
        },
        error: function () {
        }
    });
};

window.addFacilitateur = function (e) {
    e.preventDefault();
    $.ajax({
        url:  window.location.origin + "/inscription/new",
        type: "POST",
        data: {
            nom: $('#nom').val(),
            prenom: $('#prenom').val(),
            email: $('#email').val(),
            entreprise: $('#entreprise').val()
        },
        success: function (data) {
            $.notifyDefaults({
                placement: {
                    from: "top",
                    align: "right"
                },
                // offset: {
                //     y : 55
                // },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                type: 'success',
                allow_dismiss: false
            });
            $('#createFacilitateur').modal('hide');
            $.notify("Le facilitateur a bien été rajouté. Un mail a été envoyé au facilitateur!");
            setTimeout(function(){
                window.location.reload(1);
            }, 1000);

        },
        error: function () {
            $.notifyDefaults({
                placement: {
                    from: "top",
                    align: "right"
                },
                offset: {
                    y : 55
                },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                type: 'danger',
                allow_dismiss: false
            });
            $('#createFacilitateur').modal('hide');
            $.notify('Une erreur est survenue, merci de réesayer !');
        }
    });
};

window.dellFacilitateur = function (idFacilitateur, e) {
    e.preventDefault();
    $.ajax({
        url:  window.location.origin + "/delete/facilitateur/" + idFacilitateur,
        type: "POST",
        success: function (data) {
            $.notifyDefaults({
                placement: {
                    from: "top",
                    align: "right"
                },
                // offset: {
                //     y : 55
                // },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                type: 'danger',
                allow_dismiss: false
            });
            $('.hideModal').modal('hide');
            $.notify('Le facilitateur a bien été supprimé !');
            setTimeout(function(){
                window.location.reload(1);
            }, 1000);
            // document.location.reload();
        },
        error: function () {
            $('.hideModal').modal('hide');
            $.notifyDefaults({
                placement: {
                    from: "top",
                    align: "right"
                },
                offset: {
                    y : 55
                },
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                type: 'danger',
                allow_dismiss: false
            });
            $.notify('Une erreur est survenue, merci de réesayer !');
            console.log("erreur delete facilitateur dans dataTable")
        }
    });
};