//permet d'ajouter un nouveau participant (dans le cas ou le participant s'inscrit de lui même)
//s'appel dans la modal d'ajout d'un participant sur l'index ("/")
window.addParticipant = function(id) {
    $.ajax({
        url: window.location.origin + "/create/participant/session/" + id,
        type: "POST",
        data: {
            nom: $("#nomP" + id).val(),
            prenom: $("#prenomP" + id).val(),
            email: $("#emailP" + id).val(),
            tel: $("#tel" + id).val()
        },
        success: function() {
            document.location.href = window.location.origin;
        },
    });
};

//permet d'ajouter un nouveau participant (dans le cas où le participant n'est pas inscrit et qu'il ce présente)
//s'appel dans la modal d'ajout d'un participant sur la page d'une session en cours
window.addLiveParticipant = function(id) {
    $.ajax({
        url: window.location.origin + "/create/live/participant/session/" + id,
        type: "POST",
        data: {
            nom: $("#nomParticipant").val(),
            prenom: $("#prenomParticipant").val(),
            email: $("#emailParticipant").val(),
            tel: $("#telParticipant").val()
        },
        success: function(datas) {
            getCard(datas);
        },
    });
};

//permet de reconstruir les cartes des participants aprés un ajout ou une validation de présence
window.getCard = function(datas) {
    $("#cardParticipant").empty();
    var participants = datas;
    for (var i in participants) {
        var participant = participants[i];
        $("#cardParticipant").append($("<div>").addClass("card card-fac-session col-sm-5 col-md-5 col-lg-5")
            .append($("<div>").addClass("text-center").attr("id", "bodyCard" + participant.id)
                .append($("<p>").addClass("card-title nameMemberBlack").text(participant.nom))
                .append($("<p>").addClass("card-title nameMemberBlack").text(participant.prenom))
                .append($("<hr>"))));
        if (!participant.isPresent) {
            $("#bodyCard" + participant.id).append($("<button>").addClass("btn btn-outline-primary btn-sm btn-presence").text("Présence").attr("onclick", "validerPresence(" + participant.id + "," + participant.idSession + ")"));
        } else {
            $("#bodyCard" + participant.id).append($("<span>")
                .append($("<i>")).addClass("fas fa-check fa-2x text-center"));
        }
    }
};

//permet de confirmer la présence d'un participant à une session
window.validerPresence = function(idP, idS) {
    $.ajax({
        url: window.location.origin + "/validate/participant/" + idP + "/session/" + idS,
        type: "POST",

        success: function(data) {
            getCard(data);
        },
    });
};