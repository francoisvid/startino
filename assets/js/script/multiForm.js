//permet de construir le formulaire de creation d'une seance
function getSeanceForm() {
    var id = $(".session").attr('id');
    //permet de récupérer les infos nécessaire afin de créer une seance
    $.ajax({
        url: window.location.origin + "/get/create/seance/modal/session/" + id,
        type: "GET",

        success: function(data) {
            //voir la fonction createSeance() dans le SessionController
            var session = data[0];
            var ateliers = data[1];
            //reconstruction du fomulaire de création d'une seance
            $("#seanceForm").empty();
            $("#seanceForm").append($("<label>").text("Lieu :"))
                .append($("<input>").attr("type", "text").attr("name", "adresse").attr("id", "adresse").attr("placeholder", "numero rue, code_postal, ville").attr("required", true))
                .append($("<label>").text("Selectionnez le jour :"))
                .append($("<input>").attr("type", "date").attr("name", "date").attr("id", "date").attr("required", true).attr("min", session[0].date_debut).attr("max", session[0].date_fin))
                .append($("<label>").text("Heure de debut :"))
                .append($("<input>").attr("type", "time").attr("name", "heure_debut").attr("id", "heure_debut").attr("required", true))
                .append($("<label>").text("Heure de fin :"))
                .append($("<input>").attr("type", "time").attr("name", "heure_fin").attr("id", "heure_fin").attr("required", true))
                .append($("<label>").text("Selectionnez un atelier:"))
                .append($("<select>").attr("type", "text").attr("name", "atelier").attr("id", "atelier").attr("required", true)
                    .append($("<option>")));
            for (var i in ateliers) {
                $("#atelier").append($("<option>").val(ateliers[i].id).text(ateliers[i].nom));
            }
        },
    });
}

//permet de reconstruir le fomulaire de cration d'un atelier
function newAtelierForm() {
    $("#atelierForm").empty();
    $("#atelierForm").append($("<label>").text("Nom :"))
        .append($("<input>").attr("id", "nom").attr("placeholder", "entrez le nom d'un atelier").attr("required", true).attr("type", "text").val(""))
        .append($("<label>").attr("required", true).text("Description :"))
        .append($("<textarea>").attr("id", "description").attr("required", true).attr("placeholder", "entrez une description").addClass("area").attr("type", "text").attr("rows", "12").val(""));
}

//permet de ajouter un nouvel atelier en base de données
window.addAtelier = function(id) {
    if ($("#nom").val() !== "" && $("#description").val() !== "") {
        console.log(id)
        $.ajax({
            url: window.location.origin + "/create/atelier/session/" + id,
            type: "POST",

            data: {
                nom: $("#nom").val(),
                description: $("#description").val()
            },

            success: function() {
                //reconstruction des formulaires
                newAtelierForm();
                getSeanceForm();
            },
        });
    }
    //reconstruction du fomulaire de création d'une seance
    getSeanceForm();
};

//permet de ajouter une nouvelle seance en base de données
window.createSeance = function(id) {
    if ($("#date").val() !== "" && $("#heure_debut").val() !== "" && $("#heure_fin").val() !== "" && $("#adresse").val() !== "" && $("#atelier").val() !== "") {
        $.ajax({
            url: window.location.origin + "/create/seance/session/" + id,
            type: "POST",

            data: {
                date: $("#date").val(),
                heure_debut: $("#heure_debut").val(),
                heure_fin: $("#heure_fin").val(),
                adresse: $("#adresse").val(),
                atelier: $("#atelier").val(),
            },

            success: function() {
                //reconstruction du fomulaire de création d'une seance
                getSeanceForm();
            }
        });
    }
};