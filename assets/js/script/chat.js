$(document).ready(function () {
    var id = $(".seance").attr("id");
    affichage_messages(id);
});

var refreshMsg;
//permet d'afficher les messages du chat
window.affichage_messages = function(id) {
    $.ajax({
        type: "GET",
        url:  window.location.origin + "/chat/get/message/seance/" + id,
        success: function (data) {
            creation_chat(data, id);
            clearTimeout(refreshMsg);
            //refresh tout les 500 milisecondes
            refreshMsg = setTimeout(function () {
                affichage_messages(id);
            }, 500);
        },
        error: function () {
            console.log("error");
        }
    });
};

//permet d'envoyer un message en base de données
window.save_message = function(id) {
    if ($("#textZone").val() !== "") {
        $.ajax({
            type: "POST",
            url: window.location.origin + "/chat/post/message/seance/" + id,
            dataType: "json",
            data: {
                msg: $("#textZone").val()
            },
            success: function () {
                //permet de fixer la scroll bar à chaque refresh
                var height = $('#msg_liste').prop("scrollHeight");
                $('div p').each(function(){
                    height += parseInt($(this).height());
                });

                height += '';

                $('div').animate({scrollTop: height});

                $("#textZone").val("");
            },
        });
    }
};

//permet de construir le chat à chaque refresh
window.creation_chat = function(datas) {
    var idF = $(".user").attr("id");
    $("#msg_liste").empty();
    for (var i in datas) {
        var message = datas[i];
        if(idF == message.idFacilitateur) {
            $("#msg_liste").append($("<div>").addClass("d-flex justify-content-start mb-4"))
                .append($("<div>").addClass("msg_cotainer")
                    .append($("<p>").text(message.prenom + " " + message.nom).addClass("font-weight-bold blockquote-footer").css("color", "white"))
                    .append($("<p>").text(message.text)));
        }
        else {
            $("#msg_liste").append($("<div>").addClass("d-flex justify-content-end mb-4"))
                .append($("<div>").addClass("msg_cotainer_send")
                    .append($("<p>").text(message.prenom + " " + message.nom).addClass("font-weight-bold blockquote-footer").css("color", "black"))
                    .append($("<p>").text(message.text)));
        }
    }

    $('#msg_liste').scrollTop($('#msg_liste').prop("scrollHeight"));
};