// Permet d'envoyer le mail pour la récupération du mot de passe
window.forgotpassword = function(e) {
    e.preventDefault();
    if ($('#email').val() !== '') {
        $.ajax({
            url: window.location.origin + "/sendmail",
            type: "POST",

            data: {
                email: $('#email').val()
            },

            error(xhr, status, error) {
                alert(xhr.responseText)
                console.log(status);
                console.log(error);
            }
        });
    }
}

// Permet décode le token, verifie le formulaire est set le nouveau mot de passe
window.setpassword = function(e) {
    e.preventDefault();
    $.ajax({
        url: "http://startino.bwb/reset",
        type: "POST",

        data: {
            password : $('#password').val(),
        },

        error(xhr, status, error){
            alert(xhr.responseText)
            console.log(status);
            console.log(error);
        }
    });
};
