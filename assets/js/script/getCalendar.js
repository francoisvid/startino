//creation de la modal d'alerte avant la suppresion d'une seance
window.getWarningSeanceModal = function(id){
    $("#getWarningModal").empty();
    $("#getWarningModal").append($("<div>").attr("id", "warningSeanceModal"+id).addClass("modal fade")
        .append($("<div>").addClass("modal-dialog").attr("role", "document")
            .append($("<div>").addClass("modal-content")
                .append($("<div>").addClass("modal-header")
                    .append($("<h4>").addClass("modal-title").attr("id", "exampleModalLabel").text("Attention !"))
                    .append($("<button>").addClass("close").attr("data-dismiss", "modal").attr("type", "button").attr("aria-label", "Close")
                        .append($("<span>").attr("aria-hidden", true).text("x"))))
            .append($("<div>").addClass("modal-body")
                .append($("<p>").text("Êtes-vous sûr de vouloir supprimer cette seance ?"))
                .append($("<button>").attr("type", "button").addClass("btn btn-success btn-sm modalDelete").attr("onclick", "deleteSeance("+id+")").text("Oui"))
                .append($("<button>").attr("type", "button").addClass("btn btn-danger btn-sm modalDelete").attr("data-dismiss", "modal").text("Non"))))));
    $('#warningSeanceModal'+id).modal('show');
};

//creation de la modal d'alerte avant la suppresion d'une session
window.getWarningSessionModal = function(id){
    $("#getWarningModal").empty();
    $("#getWarningModal").append($("<div>").attr("id", "warningSessionModal").addClass("modal fade")
        .append($("<div>").addClass("modal-dialog").attr("role", "document")
            .append($("<div>").addClass("modal-content")
                .append($("<div>").addClass("modal-header")
                    .append($("<h4>").addClass("modal-title").attr("id", "exampleModalLabel").text("Attention !"))
                    .append($("<button>").addClass("close").attr("data-dismiss", "modal").attr("type", "button").attr("aria-label", "Close")
                        .append($("<span>").attr("aria-hidden", true).text("x"))))
                .append($("<div>").addClass("modal-body")
                    .append($("<p>").text("Êtes-vous sûr de vouloir supprimer cette session ?"))
                    .append($("<button>").attr("type", "button").addClass("btn btn-success btn-sm modalDelete").attr("onclick", "deleteSession("+id+")").text("Oui"))
                    .append($("<button>").attr("type", "button").addClass("btn btn-danger btn-sm modalDelete").attr("data-dismiss", "modal").text("Non"))))));
    $('#warningSessionModal').modal('show');
};

//fonction qui permet d'éffectuer des actions dans fullCalendar
$(document).ready(function() {
    var id = $(".session").attr('id');
    $.ajax({
        url: window.location.origin + "/get/calendar/session/" +id,
        type: "GET",
        dataType: "json",

        success: function (datas) {
            //initialisation du calendrier
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                themeSystem: 'bootstrap4',
                editable: true,
                droppable: true,
                eventSources : datas,
                eventBackgroundColor: '#335480',
                eventTextColor: '#fff',
                dragRevertDuration: 0,

                //s'execute lors d'un resize d'un event (date de fin d'une session ou heure de fin d'une seance)
                eventResize: function(event) {
                    $.ajax({
                        url: window.location.origin + "/update/calendar/session/" +id,
                        type: "POST",
                        dataType: "json",
                        data: {
                            'id': event.id,
                            'idSeance': event.idSeance,
                            'title': event.title,
                            'start': "",
                            'end': event.end.format()
                        },
                    });
                },

                drop: function(date, allDay, event) {
                    // recupère l'element dropé et le stock dans un event object
                    var originalEventObject = $(this).data('eventObject');

                    // copie de l'event object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assignation des dates en paramétre afin d'effectuer les mises à jours
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;

                    var dateSelect = new Date(date);
                    var m = dateSelect.getMonth() + 1;
                    var y = dateSelect.getFullYear();
                    var d = dateSelect.getDate();
                    var h = dateSelect.getHours();
                    var min = dateSelect.getMinutes();

                    var newdate = y + "-" + m + "-" + d + " " + h + ":" + min;
                    var title = originalEventObject.title;
                    // executer le renderEvent de calendar avec notre eventObject
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                    $.ajax({
                        url: window.location.origin + "/update/calendar/session/" +id,
                        type: "POST",
                        dataType: "json",
                        data: {
                            'id': event.id,
                            'idSeance': event.idSeance,
                            'title': title,
                            'start': newdate,
                            'end': ""
                        },
                    });
                },

                //s'execute lors d'un clique sur une event (session ou seance)
                eventClick: function(event) {
                    $.ajax({
                        url: window.location.origin + "/get/modal/seance/" + event.idSeance,
                        type: "GET",
                        dataType: "json",
                        success: function(seance){
                            //creation du formulaire de la modal de modification des informations d'un atelier et d'une seance
                            //lors d'un clique
                            if(event.title === seance.title) {
                                $("#getModal").append($("<div>").attr("id", "seanceModal" + seance.id).addClass("modal fade")
                                    .append($("<div>").addClass("modal-dialog")
                                        .append($("<div>").addClass("modal-content")
                                            .append($("<div>").addClass("modal-header")
                                                .append($("<h4>").addClass("modal-title").attr("id", "modalTitle").text("Modifier l'atelier"))
                                                .append($("<a>").attr("type", "button").addClass("btn btn-info btn-sm btnAtelier").attr("href", window.location.origin + "/atelier/seance/" + seance.id).text("Accéder"))
                                                .append($("<button>").addClass("close").attr("data-dismiss", "modal").attr("aria-label", "Close")
                                                    .append($("<span>").attr("aria-hidden", true).text("x"))))
                                            .append($("<div>").addClass("modal-body").attr("id", "modalBody")
                                                .append($("<form>")
                                                    .append($("<div>").addClass("container")
                                                        .append($("<div>").addClass("form-group")
                                                            .append($("<label>").text("Nom :"))
                                                            .append($("<input>").addClass("form-control").attr("name", "nom").attr("id", "nom" + seance.id).attr("type", "text").val(seance.title)))
                                                        .append($("<div>").addClass("form-group")
                                                            .append($("<label>").text("Description :"))
                                                            .append($("<input>").addClass("form-control").attr("name", "description").attr("id", "description" + seance.id).attr("type", "text").val(seance.description)))
                                                        .append($("<div>").addClass("form-group")
                                                            .append($("<label>").text("Lieu :"))
                                                            .append($("<input>").addClass("form-control").attr("name", "adresse").attr("id", "adresse" + seance.id).attr("type", "text").val(seance.adresse)))
                                                        .append($("<div>").addClass("form-group")
                                                            .append($("<label>").text("Heure de debut :"))
                                                            .append($("<input>").addClass("form-control").attr("name", "heure_debut").attr("id", "heure_debut" + seance.id).attr("type", "time").val(seance.start)))
                                                        .append($("<div>").addClass("form-group")
                                                            .append($("<label>").text("Heure de fin :"))
                                                            .append($("<input>").addClass("form-control").attr("name", "heure_fin").attr("id", "heure_fin" + seance.id).attr("type", "time").val(seance.end))))))
                                            .append($("<div>").addClass("modal-footer")
                                                .append($("<button>").addClass("btn btn-danger btn-sm").attr("onclick", "getWarningSeanceModal("+seance.id+")").text("Supprimer"))
                                                .append($("<button>").addClass("btn btn-success btn-sm").attr("onclick", "updateSeance("+seance.id+")").text("Valider"))))));
                                $('#seanceModal' + seance.id).modal();
                            }
                        },
                    });
                    $.ajax({
                        url: window.location.origin + "/get/modal/session/" + event.id,
                        type: "GET",
                        dataType: "json",
                        success: function(session){
                            //creation du formulaire de la modal de modification des informations d'une session
                            //lors d'un clique
                            if (event.title === session.title) {
                                $("#getModal").append($("<div>").attr("id", "sessionModal").addClass("modal fade")
                                    .append($("<div>").addClass("modal-dialog")
                                        .append($("<div>").addClass("modal-content")
                                            .append($("<div>").addClass("modal-header")
                                                .append($("<h4>").addClass("modal-title").attr("id", "modalTitle").text("Modifier la session " + session.title))
                                                .append($("<button>").addClass("close").attr("data-dismiss", "modal").attr("aria-label", "Close")
                                                    .append($("<span>").attr("aria-hidden", true).text("x"))))
                                            .append($("<div>").addClass("modal-body").attr("id", "modalBody")
                                                .append($("<form>")
                                                    .append($("<div>").addClass("container")
                                                        .append($("<div>").addClass("form-group")
                                                            .append($("<label>").text("Nom :"))
                                                            .append($("<input>").addClass("form-control").attr("name", "nomm").attr("id", "nomm").attr("type", "text").val(session.title)))
                                                        .append($("<div>").addClass("form-group")
                                                            .append($("<label>").text("Date de debut :"))
                                                            .append($("<input>").addClass("form-control").attr("name", "date_debut").attr("id", "date_debut").attr("type", "date").val(session.start)))
                                                        .append($("<div>").addClass("form-group")
                                                            .append($("<label>").text("Date de fin :"))
                                                            .append($("<input>").addClass("form-control").attr("name", "date_fin").attr("id", "date_fin").attr("type", "date").val(session.end))))))
                                            .append($("<div>").addClass("modal-footer")
                                                .append($("<button>").addClass("btn btn-danger btn-sm").attr("onclick", "getWarningSessionModal("+id+")").text("Supprimer"))
                                                .append($("<button>").addClass("btn btn-success btn-sm").attr("onclick", "updateSession("+id+")").text("Valider"))))));
                                $('#sessionModal').modal();
                            }
                        },
                    });
                },

                //s'execute lorsqu'une seance ou une session à été déplacée
                eventDrop: function(event) {
                    var dateSelect = new Date(event.start);
                    var m = dateSelect.getMonth() + 1;
                    var y = dateSelect.getFullYear();
                    var d = dateSelect.getDate();
                    var h = dateSelect.getHours() - 1;
                    var i = dateSelect.getMinutes();

                    var newdate = y + "-" + m + "-" + d + " " + h + ":" + i;
                    var title = event.title;

                    //modification de la date de debut d'une session ou de la date d'une seance
                    $.ajax({
                        url: window.location.origin + "/update/calendar/session/" +id,
                        type: "POST",
                        dataType: "json",
                        data: {
                            'id': event.id,
                            'idSeance': event.idSeance,
                            'title': title,
                            'start': newdate,
                            'end': ""
                        },
                    });
                },
            });
        },
        error: function () {
            console.log("erreur fullCalendar");
        }
    });
});
