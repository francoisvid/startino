
//permet de modifier l'affichage de la vue en fonction de la taille de l'écran
function responsiveResize() {
    //de smartphone à tablette
    if (window.matchMedia("(min-width: 0px)").matches && window.matchMedia("(max-width: 994px)").matches) {
        $(".space").removeClass('col-9').addClass('col-sm-12');
        $("#calendar").removeClass('animated bounceInUp delay-2s').addClass('animated bounceInRight delay-2s');
        $(".aside").removeClass('col-3').addClass('col-sm-12 animated bounceInRight delay-3s');
        $(".card-fac-session").removeClass('col-md-12 col-lg-5');
        $(".card-fac-session").css('margin-left', '0px').css('margin-right', '10px');
        $("#test").removeClass('row-custom').addClass('row');
        $("#cardParticipant").removeClass('row-custom').addClass('row');
        $("#btn-warning").removeClass('animated bounceInUp').addClass('animated bounceInRight');
    }
    //de tablette à pc
    else if (window.matchMedia("(min-width: 994px)").matches && window.matchMedia("(max-width: 1207px)").matches) {
        $(".card-fac-session").removeClass('col-md-12 col-lg-7 col-lg-5').addClass('col-lg-9');
        $("#test").removeClass('row-custom').addClass('row');
        $("#cardParticipant").removeClass('row-custom').addClass('row');
        $("#btn-warning").removeClass('animated bounceInUp').addClass('animated bounceInRight');
    }
    //de pc à tv
    else{
        $("#calendar").removeClass('animated bounceInUp delay-2s').addClass('animated bounceInRight delay-2s');
        $(".card-fac-session").removeClass('col-md-12 col-lg-7').addClass('col-lg-5').css('margin-right', '10px');
        $("#col-12").removeClass('col-12');
        $("#test").removeClass('row-custom').addClass('row');
        $("#cardParticipant").removeClass('row-custom').addClass('row');
        $("#btn-warning").removeClass('animated bounceInUp').addClass('animated bounceInRight');
    }

}
window.addEventListener('resize', responsiveResize, false);
