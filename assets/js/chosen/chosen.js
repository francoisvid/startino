$(".chosen-select").chosen({disable_search_threshold: 10});
$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
$(".chosen-select").chosen({max_selected_options: 5});
$(".chosen-select").bind("chosen:maxselected", function () { });
$(".chosen-select").chosen({rtl: true});
$("#form_field").chosen().change();
$("#form_field").trigger("chosen:updated");
$("#form_field").chosen("destroy");
$(".chosen-select").chosen({width: "95%"});