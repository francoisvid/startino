<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AtelierRepository")
 */
class Atelier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Session", inversedBy="ateliers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $session;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="ateliers")
     */
    private $facilitateurs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Seance", mappedBy="atelier", orphanRemoval=true)
     */
    private $seances;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Idee", mappedBy="atelier", orphanRemoval=true)
     */
    private $idees;

    public function __construct()
    {
        $this->seances = new ArrayCollection();
        $this->atelierHasSeances = new ArrayCollection();
        $this->facilitateur = new ArrayCollection();
        $this->facilitateurs = new ArrayCollection();
        $this->idees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Seance[]
     */
    public function getSeances(): Collection
    {
        return $this->seances;
    }

    public function addSeance(Seance $seance): self
    {
        if (!$this->seances->contains($seance)) {
            $this->seances[] = $seance;
        }

        return $this;
    }

    public function removeSeance(Seance $seance): self
    {
        if ($this->seances->contains($seance)) {
            $this->seances->removeElement($seance);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getFacilitateurs(): Collection
    {
        return $this->facilitateurs;
    }

    public function addFacilitateur(User $facilitateur): self
    {
        if (!$this->facilitateurs->contains($facilitateur)) {
            $this->facilitateurs[] = $facilitateur;
        }

        return $this;
    }

    public function removeFacilitateur(User $facilitateur): self
    {
        if ($this->facilitateurs->contains($facilitateur)) {
            $this->facilitateurs->removeElement($facilitateur);
        }

        return $this;
    }

    /**
     * @return Collection|Idee[]
     */
    public function getIdees(): Collection
    {
        return $this->idees;
    }

    public function addIdee(Idee $idee): self
    {
        if (!$this->idees->contains($idee)) {
            $this->idees[] = $idee;
            $idee->setAtelier($this);
        }

        return $this;
    }

    public function removeIdee(Idee $idee): self
    {
        if ($this->idees->contains($idee)) {
            $this->idees->removeElement($idee);
            // set the owning side to null (unless already changed)
            if ($idee->getAtelier() === $this) {
                $idee->setAtelier(null);
            }
        }

        return $this;
    }
}
