<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181105110230 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE session_user (session_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_4BE2D663613FECDF (session_id), INDEX IDX_4BE2D663A76ED395 (user_id), PRIMARY KEY(session_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session_participants_inscrits (session_id INT NOT NULL, participant_id INT NOT NULL, INDEX IDX_7C7F327613FECDF (session_id), INDEX IDX_7C7F3279D1C3019 (participant_id), PRIMARY KEY(session_id, participant_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session_participants_presents (session_id INT NOT NULL, participant_id INT NOT NULL, INDEX IDX_B9A3203A613FECDF (session_id), INDEX IDX_B9A3203A9D1C3019 (participant_id), PRIMARY KEY(session_id, participant_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE session_user ADD CONSTRAINT FK_4BE2D663613FECDF FOREIGN KEY (session_id) REFERENCES session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session_user ADD CONSTRAINT FK_4BE2D663A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session_participants_inscrits ADD CONSTRAINT FK_7C7F327613FECDF FOREIGN KEY (session_id) REFERENCES session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session_participants_inscrits ADD CONSTRAINT FK_7C7F3279D1C3019 FOREIGN KEY (participant_id) REFERENCES participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session_participants_presents ADD CONSTRAINT FK_B9A3203A613FECDF FOREIGN KEY (session_id) REFERENCES session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session_participants_presents ADD CONSTRAINT FK_B9A3203A9D1C3019 FOREIGN KEY (participant_id) REFERENCES participant (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE participant_has_session');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE participant_has_session (partcipant_inscrit_id INT NOT NULL, participant_present_id INT NOT NULL, session_id INT NOT NULL, INDEX IDX_4D86408A8883C9A3 (partcipant_inscrit_id), INDEX IDX_4D86408AB1BBF8E6 (participant_present_id), INDEX IDX_4D86408A613FECDF (session_id), PRIMARY KEY(partcipant_inscrit_id, participant_present_id, session_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE participant_has_session ADD CONSTRAINT FK_4D86408A613FECDF FOREIGN KEY (session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE participant_has_session ADD CONSTRAINT FK_4D86408A8883C9A3 FOREIGN KEY (partcipant_inscrit_id) REFERENCES participant (id)');
        $this->addSql('ALTER TABLE participant_has_session ADD CONSTRAINT FK_4D86408AB1BBF8E6 FOREIGN KEY (participant_present_id) REFERENCES participant (id)');
        $this->addSql('DROP TABLE session_user');
        $this->addSql('DROP TABLE session_participants_inscrits');
        $this->addSql('DROP TABLE session_participants_presents');
    }
}
