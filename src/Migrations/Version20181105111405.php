<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181105111405 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user_has_seance');
        $this->addSql('DROP TABLE user_has_session');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_has_seance (user_id INT NOT NULL, seance_id INT NOT NULL, INDEX IDX_54382C79A76ED395 (user_id), INDEX IDX_54382C79E3797A94 (seance_id), PRIMARY KEY(user_id, seance_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_has_session (user_id INT NOT NULL, session_id INT NOT NULL, INDEX IDX_1EAE749AA76ED395 (user_id), INDEX IDX_1EAE749A613FECDF (session_id), PRIMARY KEY(user_id, session_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_has_seance ADD CONSTRAINT FK_54382C79A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_has_seance ADD CONSTRAINT FK_54382C79E3797A94 FOREIGN KEY (seance_id) REFERENCES seance (id)');
        $this->addSql('ALTER TABLE user_has_session ADD CONSTRAINT FK_1EAE749A613FECDF FOREIGN KEY (session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE user_has_session ADD CONSTRAINT FK_1EAE749AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }
}
