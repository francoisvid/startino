<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181105091057 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE idee (id INT AUTO_INCREMENT NOT NULL, libele_id INT DEFAULT NULL, intitule VARCHAR(255) NOT NULL, INDEX IDX_DE60E5C1ED09FF4 (libele_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE atelier_has_seance (atelier_id INT NOT NULL, seance_id INT NOT NULL, INDEX IDX_3E58499882E2CF35 (atelier_id), INDEX IDX_3E584998E3797A94 (seance_id), PRIMARY KEY(atelier_id, seance_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE seance (id INT AUTO_INCREMENT NOT NULL, contenu_id INT NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, INDEX IDX_DF7DFD0E3C1CC488 (contenu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, entreprise VARCHAR(255) NOT NULL, roles JSON NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participant (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE libele (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_has_seance (user_id INT NOT NULL, seance_id INT NOT NULL, INDEX IDX_54382C79A76ED395 (user_id), INDEX IDX_54382C79E3797A94 (seance_id), PRIMARY KEY(user_id, seance_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_has_session (user_id INT NOT NULL, session_id INT NOT NULL, INDEX IDX_1EAE749AA76ED395 (user_id), INDEX IDX_1EAE749A613FECDF (session_id), PRIMARY KEY(user_id, session_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contenu (id INT AUTO_INCREMENT NOT NULL, idee_id INT NOT NULL, text VARCHAR(255) NOT NULL, INDEX IDX_89C2003FD40D782A (idee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE atelier (id INT AUTO_INCREMENT NOT NULL, session_id INT NOT NULL, nom VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_E1BB1823613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participant_has_session (partcipant_inscrit_id INT NOT NULL, participant_present_id INT NOT NULL, session_id INT NOT NULL, INDEX IDX_4D86408A8883C9A3 (partcipant_inscrit_id), INDEX IDX_4D86408AB1BBF8E6 (participant_present_id), INDEX IDX_4D86408A613FECDF (session_id), PRIMARY KEY(partcipant_inscrit_id, participant_present_id, session_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session (id INT AUTO_INCREMENT NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE idee ADD CONSTRAINT FK_DE60E5C1ED09FF4 FOREIGN KEY (libele_id) REFERENCES libele (id)');
        $this->addSql('ALTER TABLE atelier_has_seance ADD CONSTRAINT FK_3E58499882E2CF35 FOREIGN KEY (atelier_id) REFERENCES atelier (id)');
        $this->addSql('ALTER TABLE atelier_has_seance ADD CONSTRAINT FK_3E584998E3797A94 FOREIGN KEY (seance_id) REFERENCES seance (id)');
        $this->addSql('ALTER TABLE seance ADD CONSTRAINT FK_DF7DFD0E3C1CC488 FOREIGN KEY (contenu_id) REFERENCES contenu (id)');
        $this->addSql('ALTER TABLE user_has_seance ADD CONSTRAINT FK_54382C79A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_has_seance ADD CONSTRAINT FK_54382C79E3797A94 FOREIGN KEY (seance_id) REFERENCES seance (id)');
        $this->addSql('ALTER TABLE user_has_session ADD CONSTRAINT FK_1EAE749AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_has_session ADD CONSTRAINT FK_1EAE749A613FECDF FOREIGN KEY (session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE contenu ADD CONSTRAINT FK_89C2003FD40D782A FOREIGN KEY (idee_id) REFERENCES idee (id)');
        $this->addSql('ALTER TABLE atelier ADD CONSTRAINT FK_E1BB1823613FECDF FOREIGN KEY (session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE participant_has_session ADD CONSTRAINT FK_4D86408A8883C9A3 FOREIGN KEY (partcipant_inscrit_id) REFERENCES participant (id)');
        $this->addSql('ALTER TABLE participant_has_session ADD CONSTRAINT FK_4D86408AB1BBF8E6 FOREIGN KEY (participant_present_id) REFERENCES participant (id)');
        $this->addSql('ALTER TABLE participant_has_session ADD CONSTRAINT FK_4D86408A613FECDF FOREIGN KEY (session_id) REFERENCES session (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contenu DROP FOREIGN KEY FK_89C2003FD40D782A');
        $this->addSql('ALTER TABLE atelier_has_seance DROP FOREIGN KEY FK_3E584998E3797A94');
        $this->addSql('ALTER TABLE user_has_seance DROP FOREIGN KEY FK_54382C79E3797A94');
        $this->addSql('ALTER TABLE user_has_seance DROP FOREIGN KEY FK_54382C79A76ED395');
        $this->addSql('ALTER TABLE user_has_session DROP FOREIGN KEY FK_1EAE749AA76ED395');
        $this->addSql('ALTER TABLE participant_has_session DROP FOREIGN KEY FK_4D86408A8883C9A3');
        $this->addSql('ALTER TABLE participant_has_session DROP FOREIGN KEY FK_4D86408AB1BBF8E6');
        $this->addSql('ALTER TABLE idee DROP FOREIGN KEY FK_DE60E5C1ED09FF4');
        $this->addSql('ALTER TABLE seance DROP FOREIGN KEY FK_DF7DFD0E3C1CC488');
        $this->addSql('ALTER TABLE atelier_has_seance DROP FOREIGN KEY FK_3E58499882E2CF35');
        $this->addSql('ALTER TABLE user_has_session DROP FOREIGN KEY FK_1EAE749A613FECDF');
        $this->addSql('ALTER TABLE atelier DROP FOREIGN KEY FK_E1BB1823613FECDF');
        $this->addSql('ALTER TABLE participant_has_session DROP FOREIGN KEY FK_4D86408A613FECDF');
        $this->addSql('DROP TABLE idee');
        $this->addSql('DROP TABLE atelier_has_seance');
        $this->addSql('DROP TABLE seance');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE participant');
        $this->addSql('DROP TABLE libele');
        $this->addSql('DROP TABLE user_has_seance');
        $this->addSql('DROP TABLE user_has_session');
        $this->addSql('DROP TABLE contenu');
        $this->addSql('DROP TABLE atelier');
        $this->addSql('DROP TABLE participant_has_session');
        $this->addSql('DROP TABLE session');
    }
}
