<?php

namespace App\Repository;

use App\Entity\Libele;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Libele|null find($id, $lockMode = null, $lockVersion = null)
 * @method Libele|null findOneBy(array $criteria, array $orderBy = null)
 * @method Libele[]    findAll()
 * @method Libele[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LibeleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Libele::class);
    }

//    /**
//     * @return Libele[] Returns an array of Libele objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Libele
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
