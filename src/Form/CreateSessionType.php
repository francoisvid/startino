<?php

namespace App\Form;

use App\Entity\Session;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class CreateSessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('nom',TextType::class, array(
                'label' => 'Nom de la session'
            ))

            ->add('date_debut', DateType::class, [
                'label' => 'Date de debut de la session',
                'widget' => 'single_text',
            ])

            ->add('date_fin',DateType::class, array(
                'label' => 'Date de fin de la session',
                'widget' => 'single_text',
            ))

//            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
//                $session = $event->getData();
//                $event->getForm()->add('facilitateurs', EntityType::class, array(
//                    'class' => User::class,
//                    'attr' => array('class' => 'chosen-select'),
//                    'query_builder' => function (UserRepository $er) use ($session){
//                        return $er->createQueryBuilder('u')
//                            ->join('u.cree_par', 'a')
//                            ->where('a = :admin')
//                            ->setParameter('admin', $session->getAdmin());
//                    },
//                    'label' => 'Facilitateurs',
//                    'choice_label' => function($facilitateur){
//                        $nom = $facilitateur->getNom();
//                        $prenom = $facilitateur->getPrenom();
//
//                        return $prenom.' '.$nom;
//                    },
//                    'multiple' => true,
//                ));
//            })
            ->add('Ajouter', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary btnConnexion btn-sm'),

            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Session::class,
        ]);
    }
}
