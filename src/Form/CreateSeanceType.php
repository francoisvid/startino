<?php

namespace App\Form;

use App\Entity\Seance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateSeanceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class, array(
                'label' => 'Nom de la seance'
            ))
            ->add('adresse',TextType::class, array(
                'label' => 'Lieu'
            ))
            ->add('date',DateType::class, array(
                'label' => 'Selectionnez le jour',
                'widget' => 'single_text',
            ))
            ->add('heure_debut',TimeType::class, array(
                'label' => 'Heure de debut',
                'widget' => 'single_text',
            ))
            ->add('heure_fin',TimeType::class, array(
                'label' => 'Heure de fin',
                'widget' => 'single_text',
            ))
            ->add('Ajouter', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary btnConnexion btn-sm'),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Seance::class,
        ]);
    }
}
