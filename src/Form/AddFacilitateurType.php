<?php

namespace App\Form;

use App\Entity\Session;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddFacilitateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $session = $event->getData();
                $event->getForm()->add('facilitateurs', EntityType::class, array(
                    'class' => User::class,
                    'attr' => array('class' => 'chosen-select'),
                    'query_builder' => function (UserRepository $er) use ($session){
                        return $er->findByCreated($session->getAdmin());
                    },
                    'label' => 'Facilitateurs',
                    'choice_label' => function($facilitateur){
                        $nom = $facilitateur->getNom();
                        $prenom = $facilitateur->getPrenom();

                        return $prenom.' '.$nom;
                    },
                    'multiple' => true,
                ));
            })
            ->add('Valider', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary btnValiderFac btn-sm')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Session::class,
        ]);
    }
}
