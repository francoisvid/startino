<?php

namespace App\Form;

use App\Entity\Session;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateSessionDateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_debut',DateType::class, array(
                'label' => 'Date du début de la session',
            ))

            ->add('date_fin',DateType::class, array(
                'attr' => array('type' => 'date'),
                'label' => 'Date de fin de la session',
            ))

            ->add('Valider', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary btnConnexion btn-sm')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Session::class,
        ]);
    }
}
