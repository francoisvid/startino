<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\TypeUser;
use App\Entity\User;
/**
 * Description of UserFixture
 *
 * @author julianbertrix
 */
class UserFixture extends Fixture{
    
    private $encoder;
    
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
         $this->encoder = $passwordEncoder;
    }

    //chargement des fixtures
    public function load(ObjectManager $manager){
        //appel des methodes
        $this->createUser($manager);
    }

    //creation des utilisateurs
    public function createUser($em){
        $user = new User();
        $user->setNom("Bertrix");
        $user->setPrenom("Julian");
        $user->setEmail("j.admin@gravity.com");
        $user->setPassword($this->encoder->encodePassword($user, 'admin'));
        $user->setEntreprise("gravity innovation");
        $user->setVerify(1);
        $user->setRoles(["ROLE_ADMIN"]);
        $em->persist($user);
        
//        for($i = 2; $i < 7; $i++){
//            $user2 = new User();
//            $user2->setNom("Facilitateur".$i);
//            $user2->setPrenom("Facilitateur".$i);
//            $user2->setEmail("facilitateur".$i."@gravity.com");
//            $user2->setPassword("");
//            $user2->setEntreprise("gravity innovation");
//            $user2->setVerify(1);
//            $user2->setRoles(["ROLE_FACILITATEUR"]);
//            $em->persist($user2);
//        }
        
        $em->flush();
    }
}
