<?php

namespace App\Controller;

use App\Form\CreateSessionType;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Session;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfilAdminController extends AbstractController
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->encoder = $passwordEncoder;
    }

    //affichage de la vue
    public function openInnovation(AuthorizationCheckerInterface $authChecker, Request $request)
    {
        if (!$authChecker->isGranted('ROLE_ADMIN'))
        {
            return $this->redirectToRoute("security_login");
        }

        //recupération des sessions et des facilitateurs ajouter par l'admin connecté
        $sessions = $this->getDoctrine()->getRepository(Session::class)->findBy(['admin'=> $this->getUser()->getId()]);
        $admin = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id'=> $this->getUser()->getId()]);
        $facilitateurs = $admin->getUsers();


        return $this->render('profil_admin/index.html.twig', [
            'sessions' => $sessions,
            'facilitateurs' => $facilitateurs
        ]);
    }

    /**
     * @Method({"POST"})
     */
    //Fonction qui permet de faire l'update de l'utilisateur connecté
    public function profilAdminRequest(Request $request, UserPasswordEncoderInterface $passwordEncoder){

        $post = $request->request->all();

        $user = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getId());
        $user->setNom($post['nom']);
        $user->setPrenom($post['prenom']);
        $user->setEmail($post['email']);
        $user->setEntreprise($post['entreprise']);
        if($request->request->get('password') !== null) {
            $user->setPassword($this->encoder->encodePassword($user, $post['password']));
            $user->setPlainPassword($this->encoder->encodePassword($user, $post['plainpassword']));
        }else{
            $user->setPassword($user->getPassword());
        }
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

        return $this->json("ok");
    }

    //Afficher la vue du formulaire de modification des informations d'un administrateur
    public function profilAdmin(){
        return $this->render('profil_admin/profil.html.twig');
    }
}
