<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{

    /**
     * Fonction qui permet de se connecter
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        if ($error){
            $this->addFlash('error', 'votre email ou votre mot de passe est incorrect');
        }

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error
        ));

    }

    /**
     * Fonction qui permet de verifier si un utilisateur est bien activé
     */
    public function verifyLogin(User $user){
        if ($user->getVerify() === 1){
            return $this->redirectToRoute('open_inno');
        }else{
            return $this->redirectToRoute('error');
        }
    }

    /**
     * La route pour se deconnecter.
     */
    public function logout(){
        return $this->redirectToRoute('index');
    }
}

