<?php

namespace App\Controller;

use App\Entity\Seance;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Session;
use App\Entity\Atelier;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class IndexController extends Controller
{
    //affichage de la vue
    public function index(AuthorizationCheckerInterface $authChecker)
    {
        //recupération des session, ateliers et seances
        $sessions = $this->getDoctrine()->getRepository(Session::class)->findAll();
        $ateliers = $this->getDoctrine()->getRepository(Atelier::class)->findAll();
        $seances = $this->getDoctrine()->getRepository(Seance::class)->findAll();

        return $this->render('index/index.html.twig', [
            'sessions' => $sessions,
            'ateliers' => $ateliers,
            'seances' => $seances,
        ]);
    }
}
