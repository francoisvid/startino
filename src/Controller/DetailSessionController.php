<?php

namespace App\Controller;

use App\Entity\Participant;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Session;
use App\Entity\Seance;
use App\Entity\User;

class DetailSessionController extends AbstractController
{
    // Permet d'envoyer tout le details necessaire a une session dans la vue
    public function detailSession($id)
    {
        //recupération de la session, des seances classées par date et des participant classés par ordre alphabetique
        $session = $this->getDoctrine()->getRepository(Session::class)->find($id);
        $seances = $this->getDoctrine()->getRepository(Seance::class)->findAllByDate();
        $participant = $this->getDoctrine()->getRepository(Participant::class)->findBy(['session' => $id], ['prenom' => 'ASC']);

        return $this->render('detail_session/index.html.twig', [
            'session' => $session,
            'seances' => $seances,
            'participantSession' => $participant,
        ]);
    }
}
