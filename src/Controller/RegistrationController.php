<?php

namespace App\Controller;

use App\Form\NewPasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Entity\Contact;
use App\Entity\TypeUser;
use App\Entity\AddBy;
use App\Entity\User;
use App\Entity\Session;
use App\Form\PasswordType;
use App\Form\AdminType;
use App\Form\UserType;
use Firebase\JWT\JWT;
use http\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class RegistrationController extends AbstractController
{

    /**
     *
     * @var int durée du token. Par défaut 24h
     */
    private $expiration = (60 * 60);

    /**
     *
     * @var array informations qui transitent entre le client et le serveur
     */
    private $payload;

    /**
     *
     * @var string La passphrase (il est important de la passer dans un
     * mecanisme de hachage pour plus de securité
     *
     */
    private $passport = "hello a tous";


    // Fonction sert a un administrateur de s'inscrire
    public function registerAdmin(Request $request, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer)
    {   
        // 1) Création du nouvelle utilisateur
        $user = new User();
        $form = $this->createForm(AdminType::class, $user);

        $form->handleRequest($request);

        // 2) Verifie si le formulaire est valide
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $nom = $form['nom']->getData();
                $prenom = $form['prenom']->getData();
                $email = $form['email']->getData();
                $entreprise = $form['entreprise']->getData();

                // 3) hash le mot de passe est fabrique la formulaire grace au données saisis
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
                $user->setNom($nom);
                $user->setPrenom($prenom);
                $user->setEmail($email);
                $user->setEntreprise($entreprise);
                $user->setRoles(['ROLE_ADMIN']);
                $user->setVerify(false);


                // 4) Enregistre l'utilisateur en BDD
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                // 5) création de l'email
                $token = $this->generateToken($user);
                $lien = "http://" . $_SERVER['SERVER_NAME'] . "/verification/?token=$token";
                $message = (new \Swift_Message('Validation Inscription'))
                    ->setFrom(['marketing.gravityinnovation@gmail.com' => 'Gravity Innovation'])
                    ->setTo($email)
                    ->setBody(
                        $this->renderView(
                        // templates/emails/registration.html.twig
                            'emails/registration.html.twig',
                            array('nom' => $nom, 'tkn' => $token, 'lien' => $lien)
                        ),
                        'text/html'
                    );


                // 6) envoi de l'email
                $mailer->send($message);
                $this->addFlash('success', 'Merci pour votre inscription, un mail vous a été envoyé');
                dump($message);
                return $this->render('security/login.html.twig');
            } else {
                $this->addFlash('error', 'Un erreur est survenu, veuillez ressayer ');
            }
        }

        return $this->render(
            'registration/admin.html.twig',
            array('form' => $form->createView())
        );
    }

    // Fonction sert a un administrateur d'inscrire un facilitateur
    public function registerFacilitateur(Request $request, AuthorizationCheckerInterface $authChecker, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer)
    {
        $post = $request->request->all();

        if (!$authChecker->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute("index");
        }

        $repoUser = $this->getDoctrine()->getRepository(User::class);
        $admin = $repoUser->findOneBy(['id' => $this->getUser()->getId()]);
        $sessions = $this->getDoctrine()->getRepository(Session::class)->findAll();
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $emAdmin = $this->getDoctrine()->getManager();
//        $form->handleRequest($request);
//        if ($form->isSubmitted()) {
//            if ($form->isValid()) {

//                $nom = $post['nom'];
//                $prenom = $post['prenom'];
//                $email = $post['email'];
//                $entreprise = $post['entreprise'];

                // set des infos pour le facilitateur
                $user->addCreePar($emAdmin->find(User::class, $admin->getId()));
                $password = $passwordEncoder->encodePassword($user, 'unmotdepassrandom');
                $user->setPassword($password);
                $user->setNom($post['nom']);
                $user->setPrenom($post['prenom']);
                $user->setEmail($post['email']);
                $user->setEntreprise($post['entreprise']);
                $user->setVerify(0);
                $user->setRoles(['ROLE_FACILITATEUR']);

                $email = $post['email'];
                $nom = $post['nom'];


            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // 5) création de l'email
            $token = $this->generateToken($user);
            $lien = "http://" . $_SERVER['SERVER_NAME'] . "/reset/?token=$token";
            $message = (new \Swift_Message('Connexion a la session'))
                ->setFrom(['marketing.gravityinnovation@gmail.com' => 'Gravity Innovation'])
                ->setTo($email)
                ->setBody(
                    $this->renderView(
                    // templates/emails/registration.html.twig
                        'emails/connexion.html.twig',
                        array('nom' => $nom, 'tkn' => $token, 'lien' => $lien)
                    ),
                    'text/html'
                );


            // 6) envoi de l'email
            $mailer->send($message);
            $this->addFlash('success', 'Merci pour votre inscription, un mail vous a été envoyé');
//            return $this->redirectToRoute('register_facilitateur');
//            } else {
//                $this->addFlash('error', 'Un erreur est survenu, veuillez ressayer ');
//            }
////            return $this->redirectToRoute('index');
//        }

        return $this->json('success');
    }


    // Fonction qui permet de générer un token
    public function generateToken(User $user)
    {

//        $key = "hello a tous";
        // stockage des informations a envoyer dans le token
        $payload = array(
            "id" => $user->getId(),
            "email" => $user->getEmail(),
            "exp" => time() + $this->expiration
        );

        // encodage du token
        $tkn = JWT::encode($payload, $this->passport);
        // création du cookie
        setcookie("tkn", $tkn, $this->payload['exp'], "http://" . $_SERVER['SERVER_NAME']);
        return $tkn;

    }

    // Fonction qui permet de decoder le token et donc de validé un utilisateur si tout est ok
    public function verifyToken(Request $request)
    {
        // recuperation du token envoyé par le serveur
        $jwt = ($request->query->get('token'));
        try {
            // On decode le token, si il tout est ok, on set la verificaztion de l'user a 1
            // Sinon on soulève une exception et on fait une redirection
            $decode = JWT::decode($jwt, $this->passport, array('HS256'));
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $decode->email]);
            $user->setVerify(true);
            $this->getDoctrine()->getManager()->merge($user);
            $this->getDoctrine()->getManager()->flush();

            $this->registerAction($user);


        } catch (Exception $ex) {
            return $this->render('index/index.html.twig');
        }
        dump($decode);
        return $this->render('index/index.html.twig');
    }

    // Fonction qui permet de se connecter apres verification par email
    public function registerAction(User $user)
    {
//        $user = //Handle getting or creating the user entity likely with a posted form
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->container->get('security.token_storage')->setToken($token);
        $this->container->get('session')->set('_security_main', serialize($token));

        return $this->redirectToRoute('index');

        // The user is now logged in, you can redirect or do whatever.
    }


    // Fonction qui permet d'envoyé le mail pour reset le password
    /**
     * @Method({"POST"})
     */
    public function resetPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer)
    {
        $post = $request->request->all();
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $post['email']]);
        $token = $this->generateToken($user);
        $link = "http://" . $_SERVER['SERVER_NAME'] . "/reset/?token=$token";
        $message = (new \Swift_Message('Reset mot de passe'))
            ->setFrom(['marketing.gravityinnovation@gmail.com' => 'Gravity Innovation'])
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'emails/password.html.twig',
                    array('nom' => $user->getNom(), 'tkn' => $token, 'link' => $link)
                ),

                'text/html'
            );
        $user->setPassword('');
        $this->getDoctrine()->getManager()->merge($user);
        $this->getDoctrine()->getManager()->flush();

        // 6) envoi de l'email
        $mailer->send($message);
        dump($token);

        return $this->render('security/resetpassword.html.twig');

    }

    // Fonction permet de setter le nouveau mot de passe en verifiant que les 2 champs sont bien identique
    /**
     * @Method({"POST"})
     */
    public function setPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {

        $jwt = ($request->query->get('token'));
        $decode = JWT::decode($jwt, $this->passport, array('HS256'));

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $decode->email]);
        $form = $this->createForm(NewPasswordType::class, $user);

        $form->handleRequest($request);

        // 2) Verifie si le formulaire est valide
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                // 3) hash le mot de passe est set le nouveaux mot de passe saisis
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);


                // 4) Enregistre l'utilisateur en BDD
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
                $this->addFlash('success', 'Felicitation, votre mot de passe a bien été modifié');
                return $this->redirectToRoute('security_login');
            }else{
                $this->addFlash('error', "Désolé, une erreur s'est produite, merci de réesayer plus tard");
            }
        }
        dump($decode);
        return $this->render(
            'security/setpassword.html.twig',
            array('form' => $form->createView(), 'user' => $user)
        );
    }
}