<?php
/**
 * Created by PhpStorm.
 * User: vidalfrancois
 * Date: 29/10/2018
 * Time: 22:35
 */

namespace App\Controller;


use App\Entity\Atelier;
use App\Entity\Contenu;
use App\Entity\Seance;
use App\Entity\Session;
use App\Form\CreateSeanceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class Formulaire extends AbstractController
{
    // Vue pour set une adresse mail pour le mdp oublié
    public function setMailForgot()
    {
        $this->addFlash('success', 'Un email de réinitisalitation viens de vous etre envoyé');
        return $this->render('security/resetpassword.html.twig');
    }

    // Retourne la vue pour set un nouveau mdp
    public function newPassword()
    {
        return $this->render('security/setpassword.html.twig');
    }
}